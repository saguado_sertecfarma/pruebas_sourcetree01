(*=============================================================================
  SERGIO AGUADO
    Proyecto solo para practicar con SourceTree.
    Probando funcionalidad SourceTree.
===============================================================================*)
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    Label2: TLabel;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function  GetAppVersion:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
   memo1.lines.add('Hola mundo!');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   Memo1.Clear;
end;

function  TForm1.GetAppVersion:string;
var
 Size, Size2: DWord;
 Pt, Pt2: Pointer;
begin
     Size := GetFileVersionInfoSize(PChar (ParamStr (0)), Size2);
     if Size > 0 then
     begin
          GetMem (Pt, Size);
          try
             GetFileVersionInfo (PChar (ParamStr (0)), 0, Size, Pt);
             VerQueryValue (Pt, '\', Pt2, Size2);
             with TVSFixedFileInfo (Pt2^) do
             begin
                  Result:= 'PruebasSourceTree01 rama master vr:'+
                           IntToStr (HiWord (dwFileVersionMS)) + '.' +
                           IntToStr (LoWord (dwFileVersionMS)) + '.' +
                           IntToStr (HiWord (dwFileVersionLS)) + '.' +
                           IntToStr (LoWord (dwFileVersionLS));
             end;
          finally
            FreeMem (Pt);
          end;
     end;
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
   Form1.Caption := GetAppVersion;
end;

end.
